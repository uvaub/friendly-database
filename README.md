# README #

This repository enables Adlib applications to acces designated Aleph databases as a 'Friendly Database' 

### What is this repository for? ###

* Enabling Adlib clients to access remote database by means of an url with this script
* 1.0 (Initial version)

### How do I get set up? ###

* Edit script configuration
* Install script on a webserver accessible from an Adlib workstation
* Define above URL as a 'Friendly Database' in Adlib designer

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact